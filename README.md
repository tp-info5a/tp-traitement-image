# Traitement d'images

Martin CAZET & Nathan ARMANET

Ce repo a pour but de stocker les TP réalisés dans le cadre de l'UE traitement d'images

## TP 1 : Echantillonnage & quantification

[sujet du TP](./TP1/TP1.pdf), [code](./TP1/code.sce) et [rapport](./TP1/rapport.pdf)

## TP 2 : Histogramme

[sujet du TP](./TP2/TP2.pdf), [code](./TP2/code.sce) et [rapport](./TP2/rapport.pdf)

## TP 3 : Filtres linéaires

[sujet du TP](./TP3/TP3.pdf), [code](./TP3/code.sce) et [rapport](./TP3/rapport.pdf)

## TP 4 : Filtres non linéaires

[sujet du TP](./TP4/TP4.pdf), [code](./TP4/code.sce) et [rapport](./TP4/rapport.pdf)

## TP 5 : Transformée de Fourier

[sujet du TP](./TP5/TP5.pdf), [code](./TP5/code.sce) et [rapport](./TP5/rapport.pdf)