// Installation des packages requis
atomsSystemUpdate();
atomsInstall('IPCV');

clear();

// 1 - Arbre à chat
chatImage = imread('chat.jpg')
arbreImage = imread('arbre.jpg')
scf(0)

chatImageHSV = rgb2hsv(chatImage)
chatImage2 = chatImageHSV(:, :, 2)

cib = bool2s(im2bw(chatImage2, 0.35))

t1 = arbreImage
t1(:, :, 1) = t1(:, :, 1) .* cib
t1(:, :, 2) = t1(:, :, 2) .* cib
t1(:, :, 3) = t1(:, :, 3) .* cib

neg = imcomplement(cib)

t2 = chatImage
t2(:, :, 1) = t2(:, :, 1) .* neg
t2(:, :, 2) = t2(:, :, 2) .* neg
t2(:, :, 3) = t2(:, :, 3) .* neg

imshow(t1+t2)

// 2 - Segmentation
scf(1)
noyauxImage = imread('noyaux.jpeg')
img21 = bool2s(im2bw(noyauxImage, 190/255))
img22 = img21 .* 255
imshow(img22)

scf(2)
thresh = imgraythresh(noyauxImage)
disp("Seuil calculé par imgraythresh : ", thresh)
img23 = bool2s(im2bw(noyauxImage, thresh))
img24 = img23 .* 255
imshow(img24)
imshow(img23)

// 3 - Dynamique
lenaImage = imread('Lena_nb.jpg')
lenaImage32 = uint32(lenaImage)

contraste_variation = (max(lenaImage32) - min(lenaImage32))/(max(lenaImage32) + min(lenaImage32))
disp("constraste par variation : ", contraste_variation)

brillance = double(sum(lenaImage32)) / double(prod(size(lenaImage32)))

disp("Brillance de l image : ", brillance)

contraste_ecartType = sqrt(double(sum((lenaImage32 - brillance)^2))/double(prod(size(lenaImage32))))

disp("constraste par ecart-type : ", contraste_ecartType)

[counts, cells] = imhist(lenaImage)

scf(3)
plot(cells, counts)
title("Histogramme de lena_nb.jpg")

// 4 - Egalisation

histo_cumul_norm = cumsum(counts)/prod(size(lenaImage))
scf(4)
plot(cells, histo_cumul_norm)
title("Histogramme cumulé et normalisé de lena_nb.jpg")

lenaImageEgal = uint8(histo_cumul_norm(lenaImage + 1) * (256 - 1))

scf(5)
imshow(lenaImageEgal)

[counts, cells] = imhist(lenaImageEgal)

scf(6)
plot(cells, counts)
title("Histogramme de l egalisation de lena")

mandrillImage = imread('mandrill.bmp')
mandrillImage16 = uint8(mandrillImage)
mandrillImageHSV = int_cvtcolor(mandrillImage16, "rgb2hsv")
// mandrillImageHSV = mandrillImageHSV / 255.00

mandrillImage2 = hsv2rgb(mandrillImageHSV)

scf(10)
imshow(mandrillImage2)
