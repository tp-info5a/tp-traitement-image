// Installation des packages requis
atomsSystemUpdate();
atomsInstall('IPCV');

clc();
clear();

function imageOuv = ouverture(typeOuverture, r, c, image, scfNbr)
    se = imcreatese(typeOuverture, r, c)
    imageOuv = imerode(imdilate(image, se), se)
    scf(scfNbr)
    imshow(imageOuv)
    title('ouverture de type '+typeOuverture)
endfunction

function imageFerm = fermeture(typeFermeture, r, c, image, scfNbr)
    se = imcreatese(typeFermeture, r, c)
    imageFerm = imdilate(imerode(image, se), se)
    scf(scfNbr)
    imshow(imageFerm)
    title('fermeture de type '+typeFermeture)
endfunction

function gradientMorphologique(typeFermeture, r, c, image, imageName, scfNbr)
    se = imcreatese(typeFermeture, r, c)
    result = imdilate(image, se) - imerode(image, se)
    scf(scfNbr)
    imshow(result)
    title('gradient de l image '+imageName+' type '+typeFermeture)
endfunction

function detection(conv, image, scfNbr, titre)
    tmp  = conv2(image, conv,  "same").^2
    tmpT = conv2(image, conv', "same").^2

    result = sqrt(tmp + tmpT)
    scf(scfNbr)
    imshow(uint8(result))
    title(titre)
endfunction

function imageBruit = bruitPoivreSel(image, pourcentage)
    value = [0, 255];
    
    nbr = prod(size(image))*pourcentage
    imageBruit = image;
    
    taille = prod(size(image))
    for i=1:nbr,
        pixel = rand() * (taille -1) +1
        imageBruit(pixel) = value(uint8(rand()*(prod(size(value)))) + 1)
    end
endfunction

function imageBruit = bruitGaussien(image, variance)
    imageBruit = image + variance*rand(image, "normal");
endfunction

function result = showconv(conv, image, scfNbr, titre)
    result = uint8(conv2(image, conv, "same"))
    scf(scfNbr)
    imshow(result)
    title(titre)
endfunction

// 1 - Fermeture

cameraman = double(imread('cameraman.jpg'));

cameraman(cameraman < 256/2)  = 0
cameraman(cameraman >= 256/2) = 255

scf(0)
imshow(cameraman)

fermeture('rect', 3, 3, cameraman, 1)
fermeture('ellipse', 3, 3, cameraman, 2)
fermeture('cross', 3, 3, cameraman, 3)
fermeture('rect', 5, 5, cameraman, 104)

// 2 - Contours

gradientMorphologique('rect', 2, 2, cameraman, 'cameraman', 4)
gradientMorphologique('ellipse', 2, 2, cameraman, 'cameraman', 5)
gradientMorphologique('cross', 2, 2, cameraman, 'cameraman', 6)

gradientMorphologique('cross', 4, 4, cameraman, 'cameraman', 107)

H5 = [0, 1, 0 ; 1, -4, 1 ;   0,  1,  0]

detection(H5, cameraman, 7, "H5")

// 3 - Débruitage

lena = imread('Lena_nb.jpg')

scf(8)
imshow(lena)

lenaPS = bruitPoivreSel(lena, 0.1)

scf(9)
imshow(lenaPS)
title("Bruit poivre-sel")

lenaG = bruitGaussien(lena, 10)

scf(10)
imshow(lenaG)
title("Bruit gaussien")

H1 = (1/9) *[1, 1, 1 ; 1, 1, 1 ; 1, 1, 1]
H2 = (1/16)*[1, 2, 1 ; 2, 4, 2 ; 1, 2, 1]

lenaPSBH1 = showconv(H1, double(lenaPS), 11, "débruitage (poivre-sel) filtre passe-bas H1")
disp("dist euclidienne poivre-sel H1 : ", sqrt(sum((double(lena)-double(lenaPSBH1)).^2)))
lenaPSBH2 = showconv(H2, double(lenaPS), 12, "débruitage (poivre-sel) filtre passe-bas H2")
disp("dist euclidienne poivre-sel H2 : ", sqrt(sum((double(lena)-double(lenaPSBH2)).^2)))
lenaGBH1 = showconv(H1, double(lenaG),  13, "débruitage (gaussien)   filtre passe-bas H1")
disp("dist euclidienne gaussien H1 : ", sqrt(sum((double(lena)-double(lenaGBH1)).^2)))
lenaGBH2 = showconv(H2, double(lenaG),  14, "débruitage (gaussien)   filtre passe-bas H2")
disp("dist euclidienne gaussien H2 : ", sqrt(sum((double(lena)-double(lenaGBH2)).^2)))

lenaPSRECTO = ouverture('rect', 3, 3, lenaPS, 15)
disp("dist euclidienne poivre-sel ouverture rect : ", sqrt(sum((double(lena)-double(lenaPSRECTO)).^2)))
lenaPSELLIPSEO = ouverture('ellipse', 3, 3, lenaPS, 16)
disp("dist euclidienne poivre-sel ouverture ellipse : ", sqrt(sum((double(lena)-double(lenaPSELLIPSEO)).^2)))
lenaPSCROSSO = ouverture('cross', 3, 3, lenaPS, 17)
disp("dist euclidienne poivre-sel ouverture cross : ", sqrt(sum((double(lena)-double(lenaPSCROSSO)).^2)))

lenaGRECTO = ouverture('rect', 3, 3, lenaG, 18)
disp("dist euclidienne gaussien ouverture rect : ", sqrt(sum((double(lena)-double(lenaGRECTO)).^2)))
lenaGELLIPSEO = ouverture('ellipse', 3, 3, lenaG, 19)
disp("dist euclidienne gaussien ouverture ellipse : ", sqrt(sum((double(lena)-double(lenaGELLIPSEO)).^2)))
lenaGCROSSO = ouverture('cross', 3, 3, lenaG, 20)
disp("dist euclidienne gaussien ouverture cross : ", sqrt(sum((double(lena)-double(lenaGCROSSO)).^2)))

lenaPSRECTF = fermeture('rect', 3, 3, lenaPS, 21)
disp("dist euclidienne poivre-sel fermeture rect : ", sqrt(sum((double(lena)-double(lenaPSRECTF)).^2)))
lenaPSELLIPSEF = fermeture('ellipse', 3, 3, lenaPS, 22)
disp("dist euclidienne poivre-sel fermeture ellipse : ", sqrt(sum((double(lena)-double(lenaPSELLIPSEF)).^2)))
lenaPSCROSSF = fermeture('cross', 3, 3, lenaPS, 23)
disp("dist euclidienne poivre-sel fermeture cross : ", sqrt(sum((double(lena)-double(lenaPSCROSSF)).^2)))

lenaGRECTF = fermeture('rect', 3, 3, lenaG, 24)
disp("dist euclidienne gaussien fermeture rect : ", sqrt(sum((double(lena)-double(lenaGRECTF)).^2)))
lenaGELLIPSEF = fermeture('ellipse', 3, 3, lenaG, 25)
disp("dist euclidienne gaussien fermeture ellipse : ", sqrt(sum((double(lena)-double(lenaGELLIPSEF)).^2)))
lenaGCROSSF = fermeture('cross', 3, 3, lenaG, 26)
disp("dist euclidienne gaussien fermeture cross : ", sqrt(sum((double(lena)-double(lenaGCROSSF)).^2)))
