// Installation des packages requis
atomsSystemUpdate();
atomsInstall('IPCV');

clc();
clear();

function showconv(conv, image, scfNbr, titre)
    result = conv2(image, conv, "same")
    scf(scfNbr)
    imshow(uint8(result))
    title(titre)
endfunction

function detection(conv, image, scfNbr, titre)
    tmp  = conv2(image, conv,  "same").^2
    tmpT = conv2(image, conv', "same").^2

    result = sqrt(tmp + tmpT)
    scf(scfNbr)
    imshow(uint8(result))
    title(titre)
endfunction

// 1 - Filtres passe haut

piano = double(imread("piano.jpg"))
monde = double(imread("monde.png"))
lena  = double(imread("Lena_nb.jpg"))

H3 = [1, 1, 1 ; 0,  0, 0 ;  -1, -1, -1]
H4 = [1, 2, 1 ; 0,  0, 0 ;  -1, -2, -1]
H5 = [0, 1, 0 ; 1, -4, 1 ;   0,  1,  0]

showconv(H3, piano, 1, "Convolution de l image piano avec H3")
showconv(H3, monde, 2, "Convolution de l image monde avec H3")
showconv(H3, lena,  3, "Convolution de l image lena avec  H3")

showconv(H3', piano, 11, "Convolution de l image piano avec H3 transposée")
showconv(H3', monde, 12, "Convolution de l image monde avec H3 transposée")
showconv(H3', lena,  13, "Convolution de l image lena avec  H3 transposée")

showconv(H4, piano, 21, "Convolution de l image piano avec H4")
showconv(H4, monde, 22, "Convolution de l image monde avec H4")
showconv(H4, lena,  23, "Convolution de l image lena  avec H4")

showconv(H4', piano, 31, "Convolution de l image piano avec H4 transposée")
showconv(H4', monde, 32, "Convolution de l image monde avec H4 transposée")
showconv(H4', lena,  33, "Convolution de l image lena  avec H4 transposée")


H3_45  = [1, 1, 0 ;  1,  0, -1 ;   0, -1, -1]
H3_135 = [0, 1, 1 ; -1,  0,  1 ;  -1, -1,  0]

showconv(H3_45, piano, 41, "Convolution de l image piano avec H3_45")
showconv(H3_45, monde, 42, "Convolution de l image monde avec H3_45")
showconv(H3_45, lena,  43, "Convolution de l image lena  avec H3_45")

showconv(H3_135, piano, 51, "Convolution de l image piano avec H3_135")
showconv(H3_135, monde, 52, "Convolution de l image monde avec H3_135")
showconv(H3_135, lena,  53, "Convolution de l image lena  avec H3_135")

detection(H5, piano, 61, "Gradient de l image piano avec H5")
detection(H5, monde, 62, "Gradient de l image monde avec H5")
detection(H5, lena,  63, "Gradient de l image lena  avec H5")


detection(H3, piano, 71, "Gradient de piano avec H3")
detection(H3, monde, 72, "Gradient de monde avec H3")
detection(H3, lena,  73, "Gradient de lena  avec H3")

detection(H4, piano, 81, "Gradient de piano avec H4")
detection(H4, monde, 82, "Gradient de monde avec H4")
detection(H4, lena,  83, "Gradient de lena  avec H4")


pianoDiff = piano - conv2(piano, H5, "same")
mondeDiff = monde - conv2(monde, H5, "same")
lenaDiff  = lena  - conv2(lena,  H5, "same")

scf(91)
imshow(uint8(pianoDiff))
title("Soustraction de la laplacienne avec l image piano")
scf(92)
imshow(uint8(mondeDiff))
title("Soustraction de la laplacienne avec l image monde")
scf(93)
imshow(uint8(lenaDiff))
title("Soustraction de la laplacienne avec l image lena")

// 2 - Filtres passe bas

H1 = (1/9) *[1, 1, 1 ; 1, 1, 1 ; 1, 1, 1]
H2 = (1/16)*[1, 2, 1 ; 2, 4, 2 ; 1, 2, 1]

showconv(H1, piano, 101, "Convolution de l image piano avec H1")
showconv(H1, monde, 102, "Convolution de l image monde avec H1")
showconv(H1, lena,  103, "Convolution de l image lena  avec H1")

showconv(H2, piano, 111, "Convolution de l image piano avec H2")
showconv(H2, monde, 112, "Convolution de l image monde avec H2")
showconv(H2, lena,  113, "Convolution de l image lena  avec H2")

