// Installation des packages requis
atomsSystemUpdate();
atomsInstall('IPCV');

clc();
clf();
clear();

function fourierImage = transformerFourier(image, nbrScf, strTitle)
    fourierImage = fftshift(fft2(image))
    fourierImageAbs = [-size(fourierImage)(1)/2:1:(size(fourierImage)(1)/2)-1]
    scf(nbrScf)
    gcf().color_map = hotcolormap(256);
    
    // On affiche l'image
    subplot(1, 2, 1)
    imshow(uint8(image))
    title("image")
    
    // On affiche la transformée de l'image
    subplot(1, 2, 2)
    Matplot(abs(fourierImage))
    title(strTitle)
endfunction

function image = getImageFromFourier(fourierImage, nbrScf, strTitle)
    scf(nbrScf)
    
    // ifft() nous donne des nombres complexes
    // on peut prendre le module de ce nombre : abs()
    image = abs(ifft(fourierImage))
    
    // on caste le module de sorte à l'afficher l'image : uint8()
    imshow(uint8(image))
    
    title(strTitle)
endfunction

function imageBruit = bruitPoivreSel(image, pourcentage)
    value = [0, 255];
    
    nbr = prod(size(image))*pourcentage
    imageBruit = image;
    
    taille = prod(size(image))
    for i=1:nbr,
        pixel = rand() * (taille -1) +1
        imageBruit(pixel) = value(uint8(rand()*(prod(size(value)))) + 1);
    end
endfunction

function imageBruit = bruitGaussien(image, variance)
    imageBruit = image + variance*rand(image, "normal");
endfunction

/*-----------------------------------------------------------------------------
                             1 - Prise en main
-----------------------------------------------------------------------------*/

sinus = double(imread('sinus.png'))
monde = double(imread('monde.png'))
lena  = double(imread('lena_nb.jpg'))
piano = double(imread('piano.png'))

// L'image piano, bien que en noir et blanc, est codé en RGB + intensité
// Ici l'intentsité est toujour au maximum (255) elle peut donc être ignorée
// Nous faisons juste une moyenne des 3 canaux pour en avoir qu'un seul.
piano_nb = (piano(:, :, 1) + piano(:, :, 2) + piano(:, :, 3))/3

fourierSinus = transformerFourier(sinus   , 0, "Transformée de Fourier de l image sinus")
fourierMonde = transformerFourier(monde   , 1, "Transformée de Fourier de l image monde")
fourierLena  = transformerFourier(lena    , 2, "Transformée de Fourier de l image lena")
fourierPiano = transformerFourier(piano_nb, 3, "Transformée de Fourier de l image piano")

/*-----------------------------------------------------------------------------
                             2 - Module et phase
-----------------------------------------------------------------------------*/

// Dans scilab :
// module d'un nbr cpx "x" = abs(x)
// phase = partie imaginaire de x = imag(x)
modulePianoPhaseLena = complex(abs(fourierPiano), imag(fourierLena))
moduleLenaPhasePiano = complex(abs(fourierLena) , imag(fourierPiano))

getImageFromFourier(modulePianoPhaseLena, 4, "Image avec le module de Piano et la phase de Lena")
getImageFromFourier(moduleLenaPhasePiano, 5, "Image avec le module de Lena et la phase de Piano")

/*-----------------------------------------------------------------------------
                             3 - Filtrage
-----------------------------------------------------------------------------*/

filtrePasseHaut = ones(fourierLena)
filtrePasseBas = zeros(fourierLena)

interval = 30

filtrePasseHaut((512-interval)/2:1:(512+interval)/2, (512-interval)/2:1:(512+interval)/2) = 0
filtrePasseBas((512-interval)/2:1:(512+interval)/2, (512-interval)/2:1:(512+interval)/2) = 1

lenaPasseHaut = fourierLena.*filtrePasseHaut
lenaPasseBas = fourierLena.*filtrePasseBas

getImageFromFourier(lenaPasseHaut, 6, "Lena avec filtre passe haut")
getImageFromFourier(lenaPasseBas, 7, "Lena avec filtre passe bas")

lenaPS = bruitPoivreSel(lena, 0.1)
lenaG = bruitGaussien(lena, 10)

fourierLenaPS = transformerFourier(lenaPS   , 8, "Transformée de Fourier de l image lena buité (poivre-sel)")
fourierLenaG  = transformerFourier(lenaG    , 9, "Transformée de Fourier de l image lena buité (gaussien)")

lenaPSPasseBas = fourierLenaPS.*filtrePasseBas
lenaGPasseBas = fourierLenaG.*filtrePasseBas

imageLenaPSPasseBas = getImageFromFourier(lenaPSPasseBas, 10, "Lena (poivre-sel) avec filtre passe bas")
disp("dist euclidienne poivre-sel passe-bas : ", sqrt(sum((lena - imageLenaPSPasseBas).^2)))

imageLenaGPasseBas = getImageFromFourier(lenaGPasseBas, 11, "Lena (gaussien) avec filtre passe bas")
disp("dist euclidienne gaussien passe-bas : ", sqrt(sum((lena - imageLenaGPasseBas).^2)))

filtre = 0.5 .* filtrePasseHaut + 1 .* filtrePasseBas

lenaFiltrePS = fourierLenaPS .* filtre
lenaFiltreG  = fourierLenaG .* filtre

imageLenaPSFiltre = getImageFromFourier(lenaFiltrePS, 12, "Lena (poivre-sel) avec filtre")
disp("dist euclidienne poivre-sel filtre : ", sqrt(sum((lena - imageLenaPSFiltre).^2)))

imageLenaGFiltre = getImageFromFourier(lenaFiltreG, 13, "Lena (gaussien) avec filtre")
disp("dist euclidienne gaussien filtre : ", sqrt(sum((lena - imageLenaGFiltre).^2)))
