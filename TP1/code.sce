// Installation des packages requis
atomsSystemUpdate();
atomsInstall('IPCV');

clear();

// 1 - Définition
ImageMandrill = imread('mandrill.bmp');

// 2 - Quantification
cameramanImage = imread('cameraman.jpg')
imshow(cameramanImage)

// niveaux de gris
qlist = [128, 64, 32, 16, 8, 4, 2];
clf;
nb = 1;
scf(0);
for i=1:length(qlist)
    q = qlist(i);
    cameramanImageDouble = double(cameramanImage)/255;
    if nb == 7
        nb = nb + 1;
    end
    d = 256/q
    subplot(3, 3, nb);
    imshow(floor(cameramanImageDouble*255/d)/(q-1));
    title(strcat([string(q), 'niveaux de gris'], ' '))
    nb = nb +1;
end


// 3 - Échantillonnage
function newImage = quantLigneColonne(image, nbrLigne, nbrColonne)
    newImage = image(1:nbrLigne:$, 1:nbrColonne:$);
endfunction
scf(1);

imshow(cameramanImageDouble);

cameramanImageDouble = double(cameramanImage)/255;
echantillon1 = quantLigneColonne(cameramanImageDouble, 2, 2);
imshow(echantillon1);

echantillon2 = quantLigneColonne(cameramanImageDouble, 8, 8);
imshow(echantillon2);
title('image quantifiée de facteur 2 et de facteur 8');

scf(5)
imout1 = imresize(echantillon1, [256 256])
subplot(1, 2, 1);
imshow(imout1)
title('Restitution de la quantification de facteur 2');

imout2 = imresize(echantillon2, [256 256])
subplot(1, 2, 2);
imshow(imout2)
title('Restitution de la quantification de facteur 8');

// 4 - Espaces colorimétriques
poolImage = imread('pool.jpg')
scf(2);
imshow(poolImage);

scf(3)
subplot(3, 2, 1);
poolImageR = poolImage;
poolImageR(:, :, 2) = zeros()
poolImageR(:, :, 3) = zeros()
imshow(poolImageR)

subplot(3, 2, 2);
imshow(poolImage(:, :, 1))

subplot(3, 2, 3);
poolImageG = poolImage;
poolImageG(:, :, 1) = zeros()
poolImageG(:, :, 3) = zeros()
imshow(poolImageG)

subplot(3, 2, 4);
imshow(poolImage(:, :, 2))

subplot(3, 2, 5);
poolImageB = poolImage;
poolImageB(:, :, 1) = zeros()
poolImageB(:, :, 2) = zeros()
imshow(poolImageB)

subplot(3, 2, 6);
imshow(poolImage(:, :, 3))

poolImageYUV = rgb2ycbcr(poolImage);
scf(4)
subplot(1, 3, 1);
imshow(poolImageYUV(:, :, 1))
title('Composante Y du format YUV');

subplot(1, 3, 2);
imshow(poolImageYUV(:, :, 2))
title('Composante U du format YUV');

subplot(1, 3, 3);
imshow(poolImageYUV(:, :, 3))
title('Composante V du format YUV');

poolImageHSV = rgb2hsv(poolImage)
scf(6)
subplot(1, 3, 1);
imshow(poolImageHSV(:, :, 1))
title('Composante H du format HSV');

subplot(1, 3, 2);
imshow(poolImageHSV(:, :, 2))
title('Composante S du format HSV');

subplot(1, 3, 3);
imshow(poolImageHSV(:, :, 3))
title('Composante V du format HSV');

// 5 - 7 différences

erreur1 = imread('erreurs_7_1.jpg');
erreur2 = imread('erreurs_7_2.jpg');

difference = uint8(255*ones(size(erreur1)(1), size(erreur1)(2), size(erreur1)(3)));
for i = 1:size(erreur1)(1),
    for j = 1:size(erreur1)(2),
        if erreur1(i, j, :) ~= erreur2(i, j, :) then
           difference(i, j, :) = min(erreur1(i, j, :), erreur2(i, j, :))
        end
    end
end

scf(7);
subplot(1, 3, 1);
imshow(erreur1);
title('Image 1')
subplot(1, 3, 2);
imshow(erreur2);
title('Image 2')
subplot(1, 3, 3);
imshow(difference)
title('Les 7 différences')
